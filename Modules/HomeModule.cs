using NancyApplication.Models;

namespace NancyApplication.Modules
{
    public class HomeModule : Nancy.NancyModule
    {
        public HomeModule()
        {
            Get("/", args => "Hello from Nancy running on CoreCLR");
            Get("/people/{name}", args => new Person { Name = args.name });
        }
    }
}
